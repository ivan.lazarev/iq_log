
import java.io.PrintWriter;
import java.io.StringWriter;
import java.util.logging.Formatter;
import java.util.logging.LogRecord;

final class LogFormatter extends Formatter {

    private static final String LINE_SEPARATOR = System.getProperty("line.separator");


    
	    @Override
	    public String format(LogRecord record) {
	    	
	    	String result="";
	        try{
	        	
		    	java.util.Date actuelle = new java.util.Date();
				java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
				String dat = dateFormat.format(actuelle);
		    	
		        StringBuilder sb = new StringBuilder();
		
		        sb.append(dat)
		            .append(" # ")
		            .append(record.getLevel())
		            .append(" ")
		            .append(formatMessage(record))
		            .append(LINE_SEPARATOR);
		
		        if (record.getThrown() != null) {
		      
		                StringWriter sw = new StringWriter();
		                PrintWriter pw = new PrintWriter(sw);
		                record.getThrown().printStackTrace(pw);
		                pw.close();
		                sb.append(sw.toString());
		        }
		        
		        result=sb.toString();
		        
	    	}catch(Throwable e){
	    		e.printStackTrace();
	    	}
	        
	        return result;
	    }

    
}