

import java.io.PrintWriter;
import java.io.StringWriter;

import java.util.logging.FileHandler;
import java.util.logging.ConsoleHandler;
import java.util.logging.Formatter;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

public class Log {

	private static String globalSystemTempPath 	= null;
	private static Level globalLogLevel			= null;

	public Log(String path, Level level){
		System.out.println("Log.Log: constructor start");
		try{
			
			setGlobalSystemTempPath(path);
			setGlobalLogLevel(level);
			
			String path_temp 		= getGlobalSystemTempPath().replace("//", "/")+"temp_%g_%u.txt";//nom du fichier + identifiant de la rotation + identifiant du processus (incremente si l'application est lancée plusieurs fois en //)
			
			System.out.println("Log.Log: path :  Interface.globalSystemTempPath : "+path_temp);
			
			if(getGlobalSystemTempPath().replace("//", "/")!=null){
				Handler fh 				= new FileHandler(path_temp, 10485760, 5, true);
				fh.setFormatter(new LogFormatter());
				
				fh.setLevel(Level.ALL);
				
				Logger.getGlobal().addHandler(fh);
			}
			
			Handler ch 				= new ConsoleHandler();
			ch.setFormatter(new LogFormatter());
			
			Logger.getGlobal().setUseParentHandlers(false);
			
			
			ch.setLevel(Level.ALL);

			Logger.getGlobal().setLevel(Level.ALL);
			
			
			Logger.getGlobal().addHandler(ch);

			
			
		}catch(Throwable e){
			
	    	java.util.Date actuelle = new java.util.Date();
			java.text.DateFormat dateFormat = new java.text.SimpleDateFormat("yyyy-MM-dd hh:mm:ss");
			String dat = dateFormat.format(actuelle);
			
			System.out.println(dat+" # ERROR Log.Log: erreur error when initialize Log ");
			e.printStackTrace();
		}

		
	}
	

	public static void log(Level log_level,String string){
	
		try{
			
			if(getGlobalSystemTempPath()==null){
				System.out.println(string);
			}else{
				if(getGlobalLogLevel().equals(Level.FINEST)){
					Logger.getGlobal().log(log_level, string);//MAC
				}else if(getGlobalLogLevel().equals(log_level)){
					Logger.getGlobal().log(log_level, string);//MAC
				}
			}
			

			
		}catch(Throwable e){
			//e.printStackTrace();
			System.out.println(string);
		}

	
	}

	public static void log(Level log_level,String string, Throwable e){
		
		try{
			if(getGlobalSystemTempPath()==null){
				System.out.println(string+e);
			}else{
				Logger.getGlobal().log(log_level, string, e);
			}
			

		}catch(Throwable t){
			//t.printStackTrace();
		}

	}


	/**
	 * @return the globalSystemTempPath
	 */
	public static String getGlobalSystemTempPath() {
		return globalSystemTempPath;
	}


	/**
	 * @param globalSystemTempPath the globalSystemTempPath to set
	 */
	private void setGlobalSystemTempPath(String globalSystemTempPath) {
		this.globalSystemTempPath = globalSystemTempPath;
	}


	/**
	 * @return the globalLogLevel
	 */
	public static Level getGlobalLogLevel() {
		return globalLogLevel;
	}


	/**
	 * @param globalLogLevel the globalLogLevel to set
	 */
	private static void setGlobalLogLevel(Level globalLogLevel) {
		Log.globalLogLevel = globalLogLevel;
	}
	
}
